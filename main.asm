%define offset 8
%define b_size 256

section .data
    %include "words.inc"
    buffer_overflow_err: db 'There is not enough buffer size for the passed string.', 0
    word_not_found_err: db 'Word not found.', 0

extern find_word
extern read_word
extern print_info 
extern print_err
extern print_newline
extern exit

global _start

section .text

_start:
    xor rax, rax
    sub rsp, b_size
    mov rdi, rsp 
    mov rsi, b_size 
    push rdi
    push rsi     
    call read_word
    pop rsi
    pop rdi
    test rax, rax
    jz .buffer_overflow
    push rdx
    mov rsi, current_node
    call find_word
    test rax, rax
    jz .word_not_found 
    pop rdx
    add rax, offset
    add rax, rdx
    add rax, 1 
    mov rdi, rax
    call print_info
    call print_newline
    mov rdi, 0 
    jmp .exit

.word_not_found:
    mov rdi, word_not_found_err 
    call print_err
    call print_newline
    mov rdi, 1 
    jmp .exit 

.buffer_overflow:
    mov rdi, buffer_overflow_err
    call print_err
    call print_newline

.exit:
    call exit 

    
